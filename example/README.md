# Example to create octopus output for reading in again

## How to run the example

* first run the ground state simulation
* then, change the variable CalculationMode to td and run the time-dependent
  simulation

Then, you can use the examples in the README file to read the data and to some
plots.
