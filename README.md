# octopus\_output

This package contains scripts to work with output from
[octopus](https://octopus-code.org), a real-space TDDFT code.
These scripts have been superseded by
[postopus](https://gitlab.com/octopus-code/postopus), but may still prove
useful for reading restart data, for example.


## Installation

You can install the scripts using the `setup.py` script to your local folder:
```
python setup.py install --user
```

This will install the required dependencies (`numpy`, `pandas`). If you want to
generate contour plots, also `matplotlib` and `scikit-image` are needed.


## API documentation

The API documentation is available [here](https://sohlmann.gitlab.io/octopus-output/).


## Usage

Examples:

* Compute maximum and minimum values of all density outputs written during SCF
  iterations of a GS calculation:
```python
from octopus_output import OctopusOutput

output = OctopusOutput()
run_mode = 'scf'
iterations = output.available_iterations(run_mode)
for i in iterations:
    density = output.get_iteration_data(run_mode, i, 'density')
    maximum = density.data.max()
    minimum = density.data.min()
    print("Iteration {}: maximum density {:e}, minimum density {:e}".format(
        i, maximum, minimum))
```

* Compute average current for all outputs from a TD simulation:
```python
from octopus_output import OctopusOutput

output = OctopusOutput()
run_mode = 'td'
iterations = output.available_iterations(run_mode)
for i in iterations:
    current_z = output.get_iteration_data(run_mode, i, 'current-z')
    print("Time step {}: average current in z directions {:e}".format(
        i, current_z.data.mean()))
    # data is available as 3D cubic array
    current_z_xy_plane = current_z.data[:, :, 0]
```

* Plot energy and laser over time for a TD calculation:
```python
import matplotlib.pyplot as plt
from octopus_output import OctopusOutput

output = OctopusOutput()

plt.figure()
plt.plot(output.td.energy.t, output.td.energy.Total)
# use pandas plot function
output.td.energy.plot('t', 'Total')
plt.figure()
plt.plot(output.td.laser.t, output.td.laser['E(1)'])
```

* Plot SCF convergence metrics (energy difference, relative density change):
```python
import matplotlib.pyplot as plt
from octopus_output import OctopusOutput

output = OctopusOutput()

plt.figure()
plt.semilogy(output.static.convergence.index, output.static.convergence.energy_diff)
plt.figure()
plt.semilogy(output.static.convergence.index, output.static.convergence.rel_dens)

# or use the convenience function
output.static.plot_convergence()
```

* Analyze GS restart data:
```python
from octopus_output import OctopusOutput
import numpy as np

output = OctopusOutput()
restart = output.get_restart_data('gs')

print("GS restart data at iteration {}.".format(restart.get_iter()))

density = restart.get_density()
print("Density at grid point [1, 4, 5]: {:e}".format(density.data[1, 4, 5]))

print("Number of wavefunctions: {}".format(restart.number_wavefunction()))
index = 1
wfn = restart.get_wavefunction(index)
wfn1 = restart.get_wavefunction(index+1)
print("Wavefunction {} at grid point [1, 4, 5]: {:e}".format(
          index, wfn.data[1, 4, 5]))

# check if the two functions are orthogonal
product = (np.conj(wfn.data)*wfn1.data).sum()
print("Product of wavefunctions {} and {}: {:e}".format(
          index, index+1, product))
```

* Plot isocontours of density differences between SCF iterations:
```python
import os
import numpy as np

from octopus_output import OctopusOutput

output = OctopusOutput()
run_mode = 'scf'
iterations = output.available_iterations(run_mode)
density = output.get_iteration_data(run_mode, iterations[0], 'density',
                                    verbose=True)
if not os.path.exists("plots"):
    os.mkdir("plots")

for i in iterations[1:]:
    density_old = density
    density = output.get_iteration_data(run_mode, i, 'density',
                                        verbose=False)
    # overwrite data in density_old -> is not needed anymore
    density_old.data[:] = (density.data - density_old.data) \
        / np.sum(np.abs(density_old.data))
    maximum = np.max(density_old.data)
    minimum = np.min(density_old.data)
    print("Iteration {}: max rel diff {:e}, min rel diff {:e}".format(
        i, maximum, minimum))
    plot_name = os.path.join("plots", "density_diff_{:04d}.png".format(i))
    if not os.path.exists(plot_name):
        print("Plotting difference for iteration {}.".format(i))
        # density_old contains the difference now
        density_old.plot_isocontours(plot_name)
```

* Choose obf or netcdf format:
```python
from octopus_output import OctopusOutput

output = OctopusOutput(format='netcdf')
run_mode = 'scf'
iterations = output.available_iterations(run_mode)
density = output.get_iteration_data(run_mode, iterations[0], 'density',
                                    verbose=True)
```
```python
from octopus_output import OctopusOutput

output = OctopusOutput(format='obf')
run_mode = 'scf'
iterations = output.available_iterations(run_mode)
density = output.get_iteration_data(run_mode, iterations[0], 'density',
                                    verbose=True)
```

* Get wavefunctions in obf format at end of GS run
```python
from octopus_output import OctopusOutput

output = OctopusOutput(format='obf')
wf1 = output.static_quantities.get("wf-st00001")
```
