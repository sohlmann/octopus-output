#!/usr/bin/env python
from setuptools import setup

setup(
    name='octopus_output',
    description='Python scripts for working with output from octopus, '
                'a real-space TDDFT code',
    author='Sebastian Ohlmann',
    version='0.1',
    packages=['octopus_output'],
    install_requires=['numpy', 'pandas'],
    extras_require={'contour': ['skikit-image', 'matplotlib']},
)
