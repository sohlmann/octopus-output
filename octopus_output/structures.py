import pandas as pd
from io import StringIO
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


class AtomicData:
    """atomic data from Wikipedia, extracted 02/2020"""
    atom_data = """1   H   hydrogen  25  53  120   38  no_data
    2   He  helium  120   31  140   32  no_data
    3   Li  lithium   145   167   182   134   no_data   152
    4   Be  beryllium   105   112   153    90  85  112
    5   B   boron   85  87  192    82  73
    6   C   carbon  70  67  170   77  60
    7   N   nitrogen  65  56  155   75  54
    8   O   oxygen  60  48  152   73  53
    9   F   fluorine  50  42  147   71  53
    10  Ne  neon  160   38  154   69  no_data
    11  Na  sodium  180   190   227   154   no_data   186
    12  Mg  magnesium   150   145   173   130   127   160
    13  Al  aluminium   125   118   184    118   111   143
    14  Si  silicon   110   111   210   111   102
    15  P   phosphorus  100   98  180   106   94
    16  S   sulfur  100   88  180   102   95
    17  Cl  chlorine  100   79  175   99  93
    18  Ar  argon   71  71  188   97  96
    19  K   potassium   220   243   275   196   no_data   227
    20  Ca  calcium   180   194   231    174   133   197
    21  Sc  scandium  160   184   211    144   114   162
    22  Ti  titanium  140   176   no_data   136   108   147
    23  V   vanadium  135   171   no_data   125   106   134
    24  Cr  chromium  140   166   no_data   127   103   128
    25  Mn  manganese   140   161   no_data   139   103   127
    26  Fe  iron  140   156   no_data   125   102   126
    27  Co  cobalt  135   152   no_data   126   96  125
    28  Ni  nickel  135   149   163   121   101   124
    29  Cu  copper  135   145   140   138   120   128
    30  Zn  zinc  135   142   139   131   no_data   134
    31  Ga  gallium   130   136   187   126   121   135
    32  Ge  germanium   125   125   211    122   114
    33  As  arsenic   115   114   185   119   106
    34  Se  selenium  115   103   190   116   107
    35  Br  bromine   115   94  185   114   110
    36  Kr  krypton   no_data   88  202   110   108
    37  Rb  rubidium  235   265   303    211   no_data   248
    38  Sr  strontium   200   219   249    192   139   215
    39  Y   yttrium   180   212   no_data   162   124   180
    40  Zr  zirconium   155   206   no_data   148   121   160
    41  Nb  niobium   145   198   no_data   137   116   146
    42  Mo  molybdenum  145   190   no_data   145   113   139
    43  Tc  technetium  135   183   no_data   156   110   136
    44  Ru  ruthenium   130   178   no_data   126   103   134
    45  Rh  rhodium   135   173   no_data   135   106   134
    46  Pd  palladium   140   169   163   131   112   137
    47  Ag  silver  160   165   172   153   137   144
    48  Cd  cadmium   155   161   158   148   no_data   151
    49  In  indium  155   156   193   144   146   167
    50  Sn  tin   145   145   217   141   132
    51  Sb  antimony  145   133   206    138   127
    52  Te  tellurium   140   123   206   135   121
    53  I   iodine  140   115   198   133   125
    54  Xe  xenon   no_data   108   216   130   122
    55  Cs  caesium   260   298   343    225   no_data   265
    56  Ba  barium  215   253   268    198   149   222
    57  La  lanthanum   195   195   no_data   169   139   187
    58  Ce  cerium  185   158   no_data   no_data   131   181.8
    59  Pr  praseodymium  185   247   no_data   no_data   128   182.4
    60  Nd  neodymium   185   206   no_data   no_data   no_data   181.4
    61  Pm  promethium  185   205   no_data   no_data   no_data   183.4
    62  Sm  samarium  185   238   no_data   no_data   no_data   180.4
    63  Eu  europium  185   231   no_data   no_data   no_data   180.4
    64  Gd  gadolinium  180   233   no_data   no_data   132   180.4
    65  Tb  terbium   175   225   no_data   no_data   no_data   177.3
    66  Dy  dysprosium  175   228   no_data   no_data   no_data   178.1
    67  Ho  holmium   175   226   no_data   no_data   no_data   176.2
    68  Er  erbium  175   226   no_data   no_data   no_data   176.1
    69  Tm  thulium   175   222   no_data   no_data   no_data   175.9
    70  Yb  ytterbium   175   222   no_data   no_data   no_data   176
    71  Lu  lutetium  175   217   no_data   160   131   173.8
    72  Hf  hafnium   155   208   no_data   150   122   159
    73  Ta  tantalum  145   200   no_data   138   119   146
    74  W   tungsten  135   193   no_data   146   115   139
    75  Re  rhenium   135   188   no_data   159   110   137
    76  Os  osmium  130   185   no_data   128   109   135
    77  Ir  iridium   135   180   no_data   137   107   135.5
    78  Pt  platinum  135   177   175   128   110   138.5
    79  Au  gold  135   174   166   144   123   144
    80  Hg  mercury   150   171   155   149   no_data   151
    81  Tl  thallium  190   156   196   148   150   170
    82  Pb  lead  180   154   202   147   137
    83  Bi  bismuth   160   143   207    146   135
    84  Po  polonium  190   135   197    no_data   129
    85  At  astatine  no_data   127   202    no_data   138
    86  Rn  radon   no_data   120   220    145   133
    87  Fr  francium  no_data   no_data   348    no_data   no_data   no_data
    88  Ra  radium  215   no_data   283    no_data   159   no_data
    89  Ac  actinium  195   no_data   no_data   no_data   140
    90  Th  thorium   180   no_data   no_data   no_data   136   179
    91  Pa  protactinium  180   no_data   no_data   no_data   129   163
    92  U   uranium   175   no_data   186   no_data   118   156
    93  Np  neptunium   175   no_data   no_data   no_data   116   155
    94  Pu  plutonium   175   no_data   no_data   no_data   no_data   159
    95  Am  americium   175   no_data   no_data   no_data   no_data   173
    96  Cm  curium  no_data   no_data   no_data   no_data   no_data   174
    97  Bk  berkelium   no_data   no_data   no_data   no_data   no_data   170
    98  Cf  californium   no_data   no_data   no_data   no_data   no_data   186+/-
    99  Es  einsteinium   no_data   no_data   no_data   no_data   no_data   186+/-
    100   Fm  fermium   no_data   no_data   no_data   no_data   no_data   no_data
    101   Md  mendelevium   no_data   no_data   no_data   no_data   no_data   no_data
    102   No  nobelium  no_data   no_data   no_data   no_data   no_data   no_data
    103   Lr  lawrencium  no_data   no_data   no_data   no_data   no_data   no_data
    104   Rf  rutherfordium   no_data   no_data   no_data   no_data   131   no_data
    105   Db  dubnium   no_data   no_data   no_data   no_data   126   no_data
    106   Sg  seaborgium  no_data   no_data   no_data   no_data   121   no_data
    107   Bh  bohrium   no_data   no_data   no_data   no_data   119   no_data
    108   Hs  hassium   no_data   no_data   no_data   no_data   118   no_data
    109   Mt  meitnerium  no_data   no_data   no_data   no_data   113   no_data
    110   Ds  darmstadtium  no_data   no_data   no_data   no_data   112   no_data
    111   Rg  roentgenium   no_data   no_data   no_data   no_data   118   no_data
    112   Cn  copernicium   no_data   no_data   no_data   no_data   130   no_data
    113   Nh  nihonium  no_data   no_data   no_data   no_data   no_data   no_data
    114   Fl  flerovium   no_data   no_data   no_data   no_data   no_data   no_data
    115   Mc  moscovium   no_data   no_data   no_data   no_data   no_data   no_data
    116   Lv  livermorium   no_data   no_data   no_data   no_data   no_data   no_data
    117   Ts  tennessine  no_data   no_data   no_data   no_data   no_data   no_data
    118   Og  oganesson   no_data   no_data   no_data   no_data   no_data   no_data
    """

    def __init__(self):
        self.data = pd.read_csv(
            StringIO(self.atom_data), sep='\s+', index_col=0, names=[
                'Z', 'Symbol', 'Element', 'empirical', 'calculated', 'vdw',
                'covalent_single', 'covalent_triple', 'metallic'],
            na_values=['no_data'])

    def get_radius(self, symbol):
        """Return radius of element in Angstrom"""
        return self.data[self.data.Symbol == symbol].empirical.values / 100.0


class Sphere:
    """Handle spheres for 3d plotting"""
    def __init__(self, center, radius=1, resolution=10, ax=None, color=None):
        self.center = center
        self.resolution = 10
        u = np.linspace(0, 2 * np.pi, resolution)
        v = np.linspace(0, np.pi, resolution)
        self.x = center[0] + radius * np.outer(np.cos(u), np.sin(v))
        self.y = center[1] + radius * np.outer(np.sin(u), np.sin(v))
        self.z = center[2] + radius * np.outer(np.ones(np.size(u)), np.cos(v))
        if ax is not None:
            self.plot(ax, color=color)

    def plot(self, ax, color=None):
        if color is None:
            color = 'b'
        ax.plot_surface(self.x, self.y, self.z, color=color)


class XYZFile:
    """Handle XYZ files and plot the corresponding structure"""
    def __init__(self, filename):
        """Read XYZ file with filename filename"""
        self.filename = filename
        self.data = pd.read_csv(filename, sep='\s+', skiprows=2,
                                names=['Element', 'x', 'y', 'z'])
        self.atomic_data = AtomicData()

    def plot(self, plotfile, scale_radii=0.9, color='gray', resolution=10):
        """Plot the structure from the XYZ file.

        Arguments:
            plotfile: Name of file to plot
            scale_radii: scale factor for sphere radii
            color: color of spheres
            resolution: resolution of spheres
        """
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        self._plot_spheres(fig, ax, scale_radii=scale_radii, color=color,
                           resolution=resolution)
        plt.tight_layout()
        fig.savefig(plotfile, dpi=300)
        plt.close(fig)

    def _plot_spheres(self, fig, ax, scale_radii, color, resolution):
        spheres = [
            Sphere([x, y, z],
                   radius=scale_radii*self.atomic_data.get_radius(el),
                   ax=ax, color=color, resolution=resolution)
            for x, y, z, el in zip(self.data.x, self.data.y, self.data.z,
                                   self.data.Element)
        ]
        self.spheres = spheres
        ax.set_xlabel('x [$\AA$]')
        ax.set_ylabel('y [$\AA$]')
        ax.set_zlabel('z [$\AA$]')
        return fig, ax
