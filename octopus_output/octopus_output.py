import os
import glob

from .grid_data import GridData
from .output_folder import OutputFolder
from .static import StaticData
from .td import TDData
from .octopus_restart import OctopusRestart
from .structures import XYZFile


class OctopusOutput:
    """Handle a full simulation output directory.

    This class checks for available data and can return restart data and data
    from iterations for different run modes as well as data from the static and
    td.general folders.
    """
    def __init__(self, base_path='./', verbose=True, format='netcdf'):
        """Check which data is available and read static and td data.

        A dictionary of the folders in the output_iter directory is saved in
        self.folders. The first output folder of a scf simulation is stored in
        self.folders['scf'][1].

        GS information from the static folder is made available in self.static.
        TD information from the td.general folder is made available in self.td.
        The initial geometry is read from exec/initial_geometry.xyz and
        available in self.initial_geometry.

        Arguments:
            base_path: base path to simulation directory
            verbose: if True, be more verbose when reading files
            format: file format to be read (default: netcdf)
        """
        self.base_path = base_path
        self.verbose = verbose

        # get grid data only once and pass it through
        self.grid_data = GridData(base_path)

        # add output from output_iter
        self.output_path = os.path.join(base_path, 'output_iter')
        folder_list = sorted(glob.glob(os.path.join(self.output_path, '*')))
        self.folders = {}
        for folder in folder_list:
            folder_name = folder.split('/')[-1]
            run_mode, number = folder_name.split('.')
            if run_mode not in self.folders:
                self.folders[run_mode] = {}
            self.folders[run_mode][int(number)] = \
                OutputFolder(folder, base_path=base_path,
                             grid_data=self.grid_data, format=format)

        self.static = StaticData(base_path, grid_data=self.grid_data)
        self.static_quantities = OutputFolder("static", base_path=base_path,
                                              grid_data=self.grid_data,
                                              format=format)
        self.td = TDData(base_path, grid_data=self.grid_data)

        # add geometry if available
        geometry_file = os.path.join(
            base_path, 'exec', 'initial_coordinates.xyz')
        if os.path.exists(geometry_file):
            self.initial_geometry = XYZFile(geometry_file)

    def available_iterations(self, run_mode):
        """Return available iterations for given run mode.

        Arguments:
            run_mode: gives the run mode for which iterations should be checked

        Returns:
            list: List of available iterations
        """
        if run_mode in self.folders:
            return list(self.folders[run_mode].keys())
        else:
            return []

    def available_quantities(self, run_mode, iteration):
        """Return available quantities for given run mode and iteration.

        Arguments:
            run_mode: gives the run mode for which quantities should be checked
            iteration: iteration index for which quantities should be checked

        Returns:
            list: List of available quantities
        """
        if run_mode in self.folders:
            return self.folders[run_mode][iteration].available_quantities()
        else:
            return []

    def get_iteration_data(self, run_mode, iteration, quantity,
                           map_to_cube=True, verbose=True):
        """Load OutputFile for a given run mode, iteration and quantity.

        Arguments:
            run_mode: gives the run mode for which quantity should be loaded
            iteration: iteration index to load
            quantity: name of quantity to load (first part of .obf file name)
            map_to_cube: if True, include mapping to cubic grid (default: True)
            verbose: print more information when reading files (default: True)

        Returns:
            OutputFile: object with requested quantity
        """
        return self.folders[run_mode][iteration].get(
            quantity, map_to_cube=map_to_cube, verbose=verbose)

    def get_restart_data(self, run_mode='td'):
        """Return restart data for the given run mode.

        Arguments:
            run_mode: gives the run mode for which restart data should be
                loaded (e.g. gs or td; default: td)

        Returns:
            OctopusRestart: object with access to the restart files
        """
        return OctopusRestart(self.base_path, run_mode, self.verbose)
