import os
import pandas as pd

from .output_folder import OutputFolder


class StaticData:
    """Handle SCF data given in the static folder"""
    _forces_columns = ['index', 'species',
                       'total_x', 'total_y', 'total_z',
                       'ion-ion_x', 'ion-ion_y', 'ion-ion_z',
                       'vdw_x', 'vdw_y', 'vdw_z',
                       'local_x', 'local_y', 'local_z',
                       'nl_x', 'nl_y', 'nl_z',
                       'fields_x', 'fields_y', 'fields_z',
                       'hubbard_x', 'hubbard_y', 'hubbard_z',
                       'scf_x', 'scf_y', 'scf_z',
                       'nlcc_x', 'nlcc_y', 'nlcc_z',
                       'phot_x', 'phot_y', 'phot_z']

    def __init__(self, base_path='./', grid_data=None):
        """Read in SCF data from the static folder

        This makes the folder available for reading output files from it (in
        self.folder). Moreover, it reads pandas dataframes for the
        convergence and forces files.

        Arguments:
            base_path: base path to simulation directory
            grid_data: if not given, read in a GridData object, otherwise,
                reuse the object given in this parameter
        """
        folder_name = 'static'
        self.folder = OutputFolder(folder_name, base_path=base_path,
                                   grid_data=grid_data)
        convergence_file = os.path.join(base_path, folder_name, 'convergence')
        if os.path.exists(convergence_file):
            self.convergence = pd.read_csv(convergence_file, sep='\s+',
                                           index_col=0)
        forces_file = os.path.join(base_path, folder_name, 'forces')
        if os.path.exists(forces_file):
            self.forces = pd.read_csv(forces_file, sep='\s+', index_col=0,
                                      header=None, skiprows=1,
                                      names=self._forces_columns)

    def plot_convergence(self, filename='convergence.png'):
        import matplotlib.pyplot as plt
        self.convergence['energy_absdiff'] = self.convergence.energy_diff.abs()
        self.convergence.plot(y=['energy_absdiff', 'rel_dens', 'rel_ev'],
                              logy=True, sharex=True, subplots=True,
                              layout=(3, 1), figsize=(6, 6*3*0.5))
        plt.tight_layout()
        plt.savefig(filename)
