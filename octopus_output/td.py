import os
import pandas as pd

from .output_folder import OutputFolder


class TDData:
    """Handle TD data given in the td.general folder"""
    # implement more general header reading?
    def __init__(self, base_path='./', folder_name='td.general', grid_data=None):
        """Read in TD data from the td.general folder

        This makes the folder available for reading output files from it (in
        self.folder). Moreover, it reads pandas dataframes for the
        energy and laser files.

        Arguments:
            base_path: base path to simulation directory
            grid_data: if not given, read in a GridData object, otherwise,
                reuse the object given in this parameter
        """
        self.folder = OutputFolder(folder_name, base_path=base_path,
                                   grid_data=grid_data)

        energy_file = os.path.join(base_path, folder_name, 'energy')
        if os.path.exists(energy_file):
            # get header columns first
            with open(energy_file, 'r') as f:
                line = f.readline()
                line = f.readline()
                line = f.readline()
            # complicated splitting needed because some column titles have a
            # space inside
            column_headers = [s.strip() for s in line[1:-1].split('  ')
                              if len(s) > 1]
            self.energy = pd.read_csv(energy_file, sep='\s+',
                                      index_col=0, skiprows=5, header=None,
                                      names=column_headers)

        current_file = os.path.join(base_path, folder_name, 'total_current')
        if os.path.exists(current_file):
            # get header columns first
            with open(current_file, 'r') as f:
                line = f.readline()
                line = f.readline()
                line = f.readline()
            # complicated splitting needed because some column titles have a
            # space inside
            column_headers = [s.strip() for s in line[1:-1].split('  ')
                              if len(s) > 1]
            self.current = pd.read_csv(current_file, sep='\s+',
                                      index_col=0, skiprows=5, header=None,
                                      names=column_headers)

        laser_file = os.path.join(base_path, folder_name, 'laser')
        if os.path.exists(laser_file):
            # get header columns first
            with open(laser_file, 'r') as f:
                line = f.readline()
                line = f.readline()
                line = f.readline()
                line = f.readline()
            # complicated splitting not needed here
            column_headers = line[1:].split()
            self.laser = pd.read_csv(laser_file, sep='\s+',
                                     index_col=0, skiprows=6, header=None,
                                     names=column_headers)
