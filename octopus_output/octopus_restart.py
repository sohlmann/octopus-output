import os

from .grid_data import GridData
from .restart_file_set import RestartFileSet
from .input_file import InputFile


class OctopusRestart:
    """Handle restart files of a octopus simulation."""
    input_names = ['states']
    file_set_names = ['density', 'occs', 'vhxc', 'wfns']

    def __init__(self, base_path='./', run_mode='td', verbose=True):
        """Check for all files in the restart directory of run_mode.

        Arguments:
            base_path: base path to simulation directory
            run_mode: run mode for which the restart files are checked; this is
                the folder name below restart/, e.g. gs or td (default: td)
            verbose: print additional information
        """
        self.base_path = base_path
        self._get_paths(base_path, run_mode, verbose)

        # get grid data only once and pass it through
        self.grid_data = GridData(base_path)

        self.inputs = {}
        for input_name in self.input_names:
            filename = os.path.join(self.restart_path, input_name)
            self.inputs[input_name] = InputFile(
                filename, base_path=self.base_path)
        self.file_sets = {}
        for file_set in self.file_set_names:
            try:
                self.file_sets[file_set] = RestartFileSet(
                    file_set, restart_path=self.restart_path,
                    base_path=self.base_path, grid_data=self.grid_data)
            except FileNotFoundError:
                continue

    def _get_paths(self, base_path, run_mode, verbose):
        if run_mode == 'td':
            restart_path = os.path.join(base_path, 'restart', 'td')
            if not os.path.exists(restart_path):
                if verbose:
                    print('TD restart files do not exist, try GS.')
                restart_path = os.path.join(base_path, 'restart', 'gs')
        elif run_mode == 'gs':
            restart_path = os.path.join(base_path, 'restart', 'gs')
        else:
            raise ValueError('Run mode {} not supported.'.format(run_mode))
        if not os.path.exists(restart_path):
            raise FileNotFoundError('No restart files found.')
        self.restart_path = restart_path
        self.partition_path = os.path.join(base_path, 'restart', 'partition')

    def get_iter(self):
        """Get number of iterations.

        Returns:
            int: Number of iterations at which this restart was written
        """
        return self.file_sets['density'].input_file['Iter']

    def number_density(self):
        """Get number of density files.

        Returns:
            int: Number of files in the density file set
        """
        return self.file_sets['density'].length

    def number_wavefunction(self):
        """Get number of wavefunction files.

        Returns:
            int: Number of files in the wavefunction file set
        """
        return self.file_sets['wfns'].length

    def number_vhxc(self):
        """Get number of xc potential files.

        Returns:
            int: Number of files in the vhxc file set
        """
        return self.file_sets['vhxc'].length

    def get_density(self, index=1):
        """Get density file with given index.

        Returns:
            OutputFile: Object with access to the requested density file
        """
        return self.file_sets['density'].get_output_file(index)

    def get_wavefunction(self, index):
        """Get wavefunction file with given index.

        Returns:
            OutputFile: Object with access to the requested wavefunction file
        """
        return self.file_sets['wfns'].get_output_file(index)

    def get_vhxc(self, index=1):
        """Get xc potential file with given index.

        Returns:
            OutputFile: Object with access to the requested vhxc file
        """
        return self.file_sets['vhxc'].get_output_file(index)
