import os


class InputFile(dict):
    """Rudimentary parser for octopus input files.

    Needed for some files in the restart folder which have the same format as
    the octopus input files.
    """
    def __init__(self, filename, base_path='./'):
        """Read input file.

        Arguments:
            filename: file name of input file
            base_path: base path to simulation directory
        """
        full_filename = os.path.join(base_path, filename)
        self.block_names = []
        self.in_block = False
        with open(full_filename, 'r') as file:
            while True:
                line = file.readline()
                if line == '':
                    break
                self.parse_line(line)

    def parse_line(self, line):
        line = line.strip()
        # skip comments starting with #
        if line[0] == '#':
            return
        # if line starts with %, we have to deal with a block
        if line[0] == '%':
            if len(line) > 1:
                self.start_block(line)
            else:
                self.end_block()
            return
        # otherwise, we have to parse the line
        if self.in_block:
            self.parse_block_line(line)
        else:
            self.parse_normal_line(line)

    def start_block(self, line):
        self.in_block = True
        self.current_block_name = line[1:]
        self[self.current_block_name] = []
        self.block_names.append(self.current_block_name)

    def end_block(self):
        self.in_block = False

    def parse_block_line(self, line):
        elements = [self.parse_element(s) for s in line.split('|')]
        self[self.current_block_name].append(elements)

    def parse_normal_line(self, line):
        name, value = line.split('=')
        self[name.strip()] = self.parse_element(value)

    def parse_element(self, element):
        element = element.strip()
        if element[0] == '"':
            # a string, remove quotes
            return element[1:-1]
        elif '.' in element:
            # if a dot is present, parse as float
            return float(element)
        else:
            # otherwise it is an integer
            return int(element)
