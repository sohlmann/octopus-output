import numpy as np

from .obf import ObfFile
from .grid_data import GridData
from .isocontours import get_plotly_isocontour, plot_isocontours


class OutputFileNetCDF:
    """Class handling an octopus output file in netcdf format."""
    def __init__(self, filename, base_path='./', verbose=True):
        """Read in octopus output file.

        Arguments:
            filename: file name of output file
            base_path: base path to simulation directory
            verbose: if True, print out some information about the output file
        """
        self.verbose = verbose
        self.base_path = base_path

        self._read_output(filename)
        # TODO: hardcode dim=3 here
        self.dim = 3

    def _read_output(self, filename):
        from netCDF4 import Dataset
        if self.verbose:
            print('Reading file {}'.format(filename))

        self.dataset = Dataset(filename)
        # netcdf returns masked arrays
        self.data = self.dataset['rdata'][:].filled()
        self.spacing = self.dataset['pos'][0, 1].filled()

    def get_density_isocontour(self):
        """Get data that can be used for plotly isocontour plots"""
        return get_plotly_isocontour(self.data, [0, 0, 0], self.spacing)

    def plot_isocontours(self, filename):
        """Plot an isocontour to filename using scikit-image marching_cubes.

        Arguments:
            filename: file name of plot to save
        """
        plot_isocontours(filename, self.data, [0, 0, 0], size=self.data.shape)
