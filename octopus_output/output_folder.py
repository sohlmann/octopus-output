import os
import glob

from .grid_data import GridData
from .output_file import OutputFile
from .output_file_netcdf import OutputFileNetCDF


class OutputFolder:
    """Handle all .obf files in a given folder"""
    def __init__(self, path, base_path='./', grid_data=None, format='netcdf'):
        """Read list of output files in path and provide easy access.

        Arguments:
            path: path to folder to search for .obf files
            base_path: base path to simulation directory
            grid_data: if not given, read in a GridData object, otherwise,
                reuse the object given in this parameter
        """
        self.base_path = base_path
        self.format = format
        if self.format == 'obf':
            file_list = sorted(glob.glob(os.path.join(path, '*.obf')))
        elif self.format == 'netcdf':
            file_list = sorted(glob.glob(os.path.join(path, '*.ncdf')))
        else:
            print(f"Error: format {format} unknown.")
        self.files = {}
        for filename in file_list:
            basename = filename.split('/')[-1]
            quantity, _ = basename.split('.')
            self.files[quantity] = filename

        if grid_data is None:
            grid_data = GridData(base_path)
        self.grid_data = grid_data

    def get(self, quantity, map_to_cube=True, verbose=True):
        """Get OutputFile object of a certain quantity in this folder.

        Arguments:
            quantity: name of quantity, the first part of the .obf file name
            map_to_cube: do a mapping to a cubic array, passed to OutputFile
            verbose: make reading of OutputFile verbose

        Returns:
            OutputFile: object with access to requested quantity
        """
        if self.format == 'obf':
            return OutputFile(self.files[quantity], base_path=self.base_path,
                              map_to_cube=map_to_cube, verbose=verbose,
                              grid_data=self.grid_data)
        elif self.format == 'netcdf':
            return OutputFileNetCDF(self.files[quantity],
                                    base_path=self.base_path, verbose=verbose)

    def available_quantities(self):
        """Get list of available quantities in this folder.

        Returns:
            list: List of strings with available quantities
        """
        return list(self.files.keys())
