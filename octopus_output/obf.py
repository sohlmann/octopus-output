import os
import struct
import numpy as np


class ObfHeader:
    """Read header of a .obf file (octopus binary format)."""
    _size_of = [4, 8, 8, 16, 4, 8]
    _base_size_of = [4, 8, 4, 8, 4, 8]
    _is_integer = [0, 0, 0, 0, 1, 1]
    _types = [np.float32, np.float64, np.complex64, np.complex128, np.int32,
              np.int64]
    _type_strings = ['float', 'double', 'complex', 'double complex', 'int',
                     'long']

    def __init__(self, header):
        """Arguments:
            header: 64-byte header
        """
        data_header = struct.unpack("7sbifldli5i", header)
        (self.text, self.version, self.one_32, self.one_f, self.one_64,
         self.one_d, self.number, self.dtype, self.extra0, self.extra1,
         self.extra2, self.extra2, self.extra4) = data_header

    def size_of(self):
        return self._size_of[self.dtype]

    def base_size_of(self):
        return self._base_size_of[self.dtype]

    def is_integer(self):
        return self._is_integer[self.dtype]

    def get_type(self):
        return self._types[self.dtype]

    def get_type_string(self):
        return self._type_strings[self.dtype]


class ObfFile:
    """Read .obf file (octopus binary format)."""
    header_length = 64

    def __init__(self, filename, base_path='./'):
        """Read .obf file given in filename.

        Arguments:
            filename: file name of .obf file to read
            base_path: base path of simulation directory
        """
        with open(os.path.join(base_path, filename), "rb") as f:
            self.header = ObfHeader(f.read(self.header_length))
            self.number_elements = self.header.number
            self.number_bytes = self.number_elements * self.header.size_of()
            self.data = np.fromfile(f, self.header.get_type(),
                                    self.number_elements)
