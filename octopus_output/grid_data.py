import os
import numpy as np

from .obf import ObfFile


class GridDump(dict):
    """Parse grid dump file with grid information"""
    def __init__(self, filename='grid', base_path='./',
                 path='restart/partition'):
        """Read and parse mesh grid file.

        As a subclass of dictionary, the information of each key-value pair is
        simply stored in the dictionary.

        Arguments:
            filename: file name of grid file, default 'grid'
            base_path: base path to simulation directory
            path: path inside simulation directory, default 'restart/partition'
        """
        with open(os.path.join(base_path, path, filename), "r") as f:
            for line in f.readlines():
                if line[0] == '#':
                    continue
                ls = line.split('=')
                self[ls[0]] = ls[1].split()


class IndexMapping:
    """Hold information about index mapping of grid."""
    def __init__(self, grid_dump, filename='indices.obf', base_path='./',
                 path='restart/partition'):
        """Read index mapping and compute shifts and sizes of the cube.

        Arguments:
            grid_dump: GridDump object with information about grid size
            filename: file name of index mapping file, default 'lxyz.obf'
            base_path: base path to simulation directory
            path: path inside simulation directory, default 'restart/partition'
        """
        np_global = int(grid_dump['np_global'][0])
        np_part_global = int(grid_dump['np_part_global'][0])
        type = int(grid_dump['type'][0])
        if type != 1:
            raise ValueError("Only index type 1 suported (cubic indices)")
        dim = int(grid_dump['dim'][0])
        self.offset = []
        self.nn = []
        for i in range(1, dim+1):
            self.offset.append(int(grid_dump[f'offset( {i})'][0]))
            self.nn.append(int(grid_dump[f'    nn( {i})'][0]))
        self.stride = []
        self.stride.append(1)
        for i in range(1, dim):
            self.stride.append(self.stride[i-1]*self.nn[i-1])

        def cubic_to_point(icubic):
            point = np.zeros(dim, dtype=np.int32)
            tmp = icubic
            for i in range(dim-1, 0, -1):
                point[i] = tmp/self.stride[i] - self.offset[i]
                tmp = tmp % self.stride[i]
            point[0] = tmp - self.offset[0]
            return point

        # read index mapping
        self.obf = ObfFile(filename, os.path.join(base_path, path))
        self.data = np.zeros((np_part_global, dim), dtype=np.int32)
        for i in range(np_part_global):
            self.data[i, :] = cubic_to_point(self.obf.data[i])[:]
        # compute index shift (in the simulation, the indices run from -N/2 to
        # N/2, but here we need the indices from 0 to N
        self.index_shift = np.min(self.data[:np_global], axis=0)
        self.index_shift_full = np.min(self.data, axis=0)  # including boundary
        # shif the mapping
        self.lxyz_shifted = (self.data - self.index_shift[None, :])[:np_global]
        self.lxyz_shifted_full = self.data - self.index_shift_full[None, :]
        # size of cube
        self.size_np = np.max(self.lxyz_shifted + 1, axis=0)
        self.size_np_part = np.max(self.lxyz_shifted_full + 1, axis=0)


class GridData:
    """Class containing information about grid and index mapping"""
    def __init__(self, base_path='./', partition_path='restart/partition'):
        """Create MeshDump, GridDump, and IndexMapping classes.

        Arguments:
            base_path: base path to simulation directory
            partition_path: path inside simulation directory, default
                'restart/partition'
        """
        try:
            self.grid_dump = GridDump(path=partition_path)
            self.index_mapping = IndexMapping(
                self.grid_dump, base_path=base_path, path=partition_path)
        except FileNotFoundError:
            print("Grid Data not found in {}".format(partition_path))
            partition_path = "restart/gs"
            self.grid_dump = GridDump(path=partition_path)
            self.index_mapping = IndexMapping(
                    self.grid_dump, base_path=base_path, path=partition_path)
