import numpy as np

from .obf import ObfFile
from .grid_data import GridData
from .isocontours import get_plotly_isocontour, plot_isocontours


class OutputFile:
    """Class handling an octopus output file in obf format."""
    def __init__(self, filename, base_path='./',
                 partition_path='restart/partition',
                 map_to_cube=True, verbose=True, grid_data=None):
        """Read octopus output file and enhance with grid information.

        If map_to_cube is True (the default), the data component will contain a
        mapping of the data to a cubic grid.

        Arguments:
            filename: file name of output file
            base_path: base path to simulation directory
            partition_path: path to partition data, needed for the grid data,
                default 'restart/partition'
            map_to_cube: if True, store a mapping to a cubic array in data
            verbose: if True, print out some information about the output file
            grid_data: if not given, read in a GridData object, otherwise,
                reuse the object given in this parameter
        """
        self.verbose = verbose
        self.base_path = base_path

        if grid_data is None:
            grid_data = GridData(base_path, partition_path)
        self._set_grid_data(grid_data)
        self._compute_grid_information()
        self._read_output(filename, map_to_cube)

    def _set_grid_data(self, grid_data):
        self._grid_dump = grid_data.grid_dump
        self.index_mapping = grid_data.index_mapping

    def _compute_grid_information(self):
        # get sizes of arrays
        self.np_global = int(self._grid_dump['np_global'][0])
        self.np_part_global = int(
            self._grid_dump['np_part_global'][0])
        # TODO: hardcode dim=3 here
        self.dim = 3
        # old version: grid size was written out, right now not accessible
        ## get size of grid
        #self.lsize = np.array(self._mesh_dump['simul_box_dump']['lsize'])\
        #    .astype(np.float64)

        ## get spacing of grid
        #self.spacing = self.lsize / self.index_mapping.size_np

    def _read_output(self, filename, map_to_cube):
        if self.verbose:
            print('Reading {} with {:d} points ({:d} with boundary) in {:d} '
                  'dimensions.'.format(filename, self.np_global,
                                       self.np_part_global, self.dim))
            grid_dimensions = " x ".join(["{:d}".format(d)
                                         for d in self.index_mapping.size_np])
            #grid_size = " x ".join(["{:f}".format(l) for l in self.lsize])
            #print('Grid dimensions are {}, the grid size is {}.'.format(
            #    grid_dimensions, grid_size))
            print('Grid dimensions are {}.'.format(grid_dimensions))

        self.obf = ObfFile(filename, self.base_path)

        if self.verbose:
            print('Read {:d} bytes (type {}).'.format(
                self.obf.number_bytes, self.obf.header.get_type_string()))

        if map_to_cube:
            if self.verbose:
                print('Mapping to cubic grid.')
            self.map_to_cube()

    def map_to_cube(self, with_boundary=False):
        if with_boundary:
            # be careful, usually the boundary is not written out!
            size_np = self.index_mapping.size_np_part
            npoints = self.np_part_global
            mapping = self.index_mapping.lxyz_shifted_full
        else:
            size_np = self.index_mapping.size_np
            npoints = self.np_global
            mapping = self.index_mapping.lxyz_shifted

        self.data = np.zeros(size_np)
        index = mapping.T
        self.data[index[0], index[1], index[2]] = self.obf.data[:npoints]

    def get_density_isocontour(self):
        """Get data that can be used for plotly isocontour plots"""
        return get_plotly_isocontour(self.data, self.index_mapping.index_shift,
                                     self.spacing)

    def plot_isocontours(self, filename):
        """Plot an isocontour to filename using scikit-image marching_cubes.

        Arguments:
            filename: file name of plot to save
        """
        plot_isocontours(filename, self.data, self.index_mapping.index_shift,
                         size=self.data.shape)
