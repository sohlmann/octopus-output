def get_plotly_isocontour(data, shift, spacing, cmap="viridis"):
    """Return list of objects to be used by plotly for a isocontour plot.

    This function uses the maching_cubes algorithm from scikit-image to compute
    the isocontours.
    """
    import numpy as np
    from matplotlib import cm
    from skimage import measure

    origin = shift * spacing

    objects = []
    factors = np.linspace(0.1, 0.9999, 5)
    for factor in factors:
        verts, faces, normals, values = measure.marching_cubes_lewiner(
            data, data.max()*factor, spacing=tuple(spacing))
        verts[:, :] += origin[None, :]
        color = cm.get_cmap(cmap)(factor, factor**3*0.95)

        surface = {}
        surface['type'] = 'mesh3d'
        surface['x'] = verts.T[0].tolist()
        surface['y'] = verts.T[1].tolist()
        surface['z'] = verts.T[2].tolist()
        surface['i'] = faces.T[0].tolist()
        surface['j'] = faces.T[1].tolist()
        surface['k'] = faces.T[2].tolist()
        surface['color'] = color[:3]
        surface['opacity'] = color[3]
        surface['colorscale'] = 'Viridis'
        objects.append(surface)
    return objects


def plot_isocontours(filename, data, shift, spacing=[1, 1, 1], size=[1, 1, 1],
                     cmap="viridis_r"):
    """Plot isocontours with matplotlib.

    This function uses the maching_cubes algorithm from scikit-image to compute
    the isocontours.

    Arguments:
        filename: file name to save plot to
        data: 3D array of data
        shift: shift of grid, needed to restore origin
        spacing: spacing of grid, needed to restore origin
        size: size of grid, needed to set axis limits
        cmap: color map to use (optional)
    """
    import matplotlib.pyplot as plt
    import numpy as np
    from matplotlib import cm
    from mpl_toolkits.mplot3d.art3d import Poly3DCollection
    from skimage import measure

    origin = shift * spacing

    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection='3d')

    two_sided = np.min(data) < 0.0
    if not two_sided:
        factors = np.linspace(0.1, 0.9999, 3)
        alphas = 1.-factors**3*0.95
    else:
        factors = [-0.999, -0.5, 0.5, 0.999]
        alphas = [0.05, 0.8, 0.8, 0.05]
        cmap = 'RdYlBu'

    def normalize_factor(factor):
        if not two_sided:
            return factor
        else:
            return (factor + 1.0) / 2.0

    for factor, alpha in zip(factors, alphas):
        if factor > 0.0:
            iso_value = factor * data.max()
        else:
            iso_value = np.abs(factor) * data.min()
        verts, faces, normals, values = measure.marching_cubes_lewiner(
            data, iso_value, spacing=tuple(spacing))
        verts[:, :] += origin[None, :]
        if two_sided:
            iso_ratio = np.abs(iso_value) / np.abs(data).max()
            if iso_ratio < np.abs(factor):
                alpha = alpha * iso_ratio/np.abs(factor)
        color = cm.get_cmap(cmap)(normalize_factor(factor), alpha)
        mesh = Poly3DCollection(verts[faces])
        # mesh.set_edgecolor('k')
        mesh.set_facecolor(color[:3])
        mesh.set_alpha(color[3])
        ax.add_collection3d(mesh)

    ax.set_xlim(-size[0]/2., size[0]/2.)
    ax.set_ylim(-size[1]/2., size[1]/2.)
    ax.set_zlim(-size[2]/2., size[2]/2.)

    fig.tight_layout()
    fig.savefig(filename)
    plt.close(fig)
