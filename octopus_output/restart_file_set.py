import os

from .grid_data import GridData
from .input_file import InputFile
from .output_file import OutputFile


class RestartFileSet:
    """Class for handling a set of restart files.

    The set is described in a file given in the same format as an octopus input
    file. The corresponding block contains the filenames and also some
    additional information. The schemata known up to now are given in
    _block_schema.
    """
    _block_schema = {
        'Real_Wavefunctions': ['k-point', 'st', 'dim', 'filename'],
        'Complex_Wavefunctions': ['k-point', 'st', 'dim', 'filename'],
        'densities': ['spin', 'nspin', 'filename'],
        'vhxc': ['spin', 'nspin', 'filename'],
        'Occupations_Eigenvalues_K-Points': [
            'occupations', 'eigenvalue', 'Im(eigenvalue)', 'k-points',
            'k-weights', 'filename', 'k-point', 'st', 'dim'],
    }

    def __init__(self, filename, restart_path, base_path='./', grid_data=None):
        """Open restart file set specified by filename.

        Arguments:
            filename: file name of the restart file set
            restart_path: path to the restart directory
            base_path: base path to simulation directory
            grid_data: can contain a GridData object; if not given, the
                GridData is loaded from the base_path folder
        """
        self.base_path = base_path
        self.restart_path = restart_path
        full_filename = os.path.join(base_path, restart_path, filename)
        self.input_file = InputFile(full_filename)
        # we expect only one blocks in these files
        if len(self.input_file.block_names) != 1:
            raise ValueError("Not exactly one block in restart file set" +
                             filename)
        self.block_name = self.input_file.block_names[0]
        self.schema = self._block_schema[self.block_name]
        self.block = self.input_file[self.block_name]
        self.length = len(self.block)
        if grid_data is None:
            grid_data = GridData(base_path)
        self.grid_data = grid_data

    def _get_file_index(self):
        return self.schema.index('filename')

    def get_output_file(self, index=1, map_to_cube=True):
        """Get output file with given index for this restart set.

        Note that the index is 1-based for compatibility with the fortran
        output files which also use 1-based indexing.

        Arguments:
            index: 1-based index of output file to load from restart set
            map_to_cube: do mapping to cube, default True

        Returns:
            OutputFile: object with access to output file
        """
        # use 1-based indexing here for compatibility with fortran!
        if index > self.length:
            raise KeyError("Error: index {} larger than number of files ({})."
                           .format(index, self.length))
        filename = self.block[index-1][self._get_file_index()] + '.obf'
        full_filename = os.path.join(self.restart_path, filename)
        return OutputFile(full_filename, base_path=self.base_path,
                          map_to_cube=map_to_cube, grid_data=self.grid_data)
