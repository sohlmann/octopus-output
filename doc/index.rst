.. include:: README.rst

The main module is :py:mod:`octopus_output`.

.. toctree::
   :maxdepth: 4
   :caption: Modules

   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
