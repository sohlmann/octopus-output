#!/usr/bin/env bash
# create documention for script library using sphinx
# dependency: sphinx (http://www.sphinx-doc.org/en/stable/)
#
# Usage: ./update_docs.sh [clean]
# if clean is given as argument, all newly created files are remove
# otherwise, the documentation is built

if [[ $1 == 'clean' ]]; then
  # set back to a clean state
  mv index.rst index.rst.bak
  rm -f *.rst
  mv index.rst.bak index.rst
  rm -rf _build _static
else
  # create rst from markdown README file
  pandoc -o README.rst ../README.md 
  # build documentation using sphinx-apidoc
  mkdir -p _build _static
  sphinx-apidoc -f -e -o ./ ../octopus_output
  make html
  echo The central html file is _build/html/index.html
fi
